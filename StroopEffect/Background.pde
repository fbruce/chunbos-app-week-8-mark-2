class Background
{
  String backgroundWord = null;//string that particular background will have
  int backgroundCount = 0;
  Colour colourFill = new Colour();
  float[] rgb = {0,0,0};

  Background(int backgroundCount)
  {
    //gets and saves string in backgroundWord attached with background count so there is one background of each word
    backgroundWord = word(backgroundCount);
    rgb = colourFill.FillColourArray();   
    rgb = colourFill.FilterColour();
  }
  String word(int ID)
  {
    switch(ID)
    {
    case 0: 
      return "Red";
    case 1: 
      return "Green";
    case 2: 
      return "Blue";
    default: 
      return "error";
    }
  }
  void draw()
  {
    background(0, 0, 0);
    rgb = colourFill.SetColour(colour);
    textFont(gameText, 50);
    fill(rgb[0], rgb[1], rgb[2]);
    text(backgroundWord, width/2, height/2-50);
  }
  void updateBackground()
  {
        rgb = colourFill.SetColour(colour);
    textFont(gameText, 50);
    fill(rgb[0], rgb[1], rgb[2]);
    text(backgroundWord, width/2, height/2-50);
  }
  float[] getScreenColour(int ID)
  {
    ID = backgroundCount;
    
    return rgb;
  }
}