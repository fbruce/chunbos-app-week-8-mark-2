class Colour
{
  float[] rgb = {0, 0, 0};

  Colour()//default constructor
  {
  }
  float [] FillColourArray()//picks random float between 0 and 1
  {
    //set red to either 0 or 1
    rgb[0] = random(0, 1);
    rgb[1] = random(0, 1);
    rgb[2] = random(0, 1);

    if (rgb[0] >= 0.66)//sets red to 1
    {

      rgb[0] = 1;
      rgb[1] = 0;
      rgb[2] = 0;
      return rgb;
    } else if (rgb[1] >= 0.66)//sets green to 1
    {
      rgb[0] = 0;
      rgb[1] = 1;
      rgb[2] = 0;
      return rgb;
    }
     else//sets blue to 1
    {
      rgb[0] = 0;
      rgb[1] = 0;
      rgb[2] = 1;
      return rgb;
    }
  }
  float[] FilterColour()//filters to 1 becomes 255
  {
    for (int i = 0; i < 3; i++)
    {
      if (rgb[i] == 1)
      {
        rgb[i] = 255;
      }
    }
    return rgb;
  }
  float[] SetColour(float[] colour)
  {
    rgb[0] = colour[0];
    rgb[1] = colour[1];
    rgb[2] = colour[2];

    return rgb;
  }
  float[] pickRandomColour(float backgroundWordColour[])
  {
    float [] randomCol = {0, 0, 0};
    int randomColour = (int)random(0, 3);
    switch(randomColour)
    {
    case 0:
      randomCol[0] = 255.0;
      randomCol[1] = 0.0;
      randomCol[2] = 0.0;
      break;
    case 1:
      randomCol[0] = 0.0;
      randomCol[1] = 255.0;
      randomCol[2] = 0.0;
      break;
    case 2:
      randomCol[0] = 0.0;
      randomCol[1] = 0.0;
      randomCol[2] = 255.0;
      break;
    }

    if (randomCol[0] == backgroundWordColour[0] || randomCol[1] == backgroundWordColour[1] || randomCol[2] == backgroundWordColour[2])
    {
      randomColour = (int)random(0, 3);
    } 
    return randomCol;
  }
}