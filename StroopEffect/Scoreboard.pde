class Scoreboard
{
  int lineLength;
  String[] lines;
  int[] intlines;
  int[] updatedLines;
  float moveTextDown = 300.0f;
  Scoreboard()
  {
    //load the text from external text document into a string array
    lines = loadStrings("scores.txt");
    //check length of string array
    lineLength = lines.length; 
    //create empty integer array
    intlines = new int[lines.length];
    //convert strings to integers for comparison later
    for (int i = 0; i < lines.length; i ++)
    {
      intlines[i] = Integer.parseInt(lines[i]);
    }
  }
  void draw()
  {
    //sort integers, this does smallest to biggest
    updatedLines = sort(intlines);
    //reverse integers, now biggest to smallest
    updatedLines =  reverse(updatedLines);
    textFont(gameText, 20);
    textAlign(CENTER, TOP);  
    fill(255, 255);
    text("Previous Top Ten Scores", width/2, height/2 - moveTextDown - 20);
    //displays scores at different positions using move text down variable
    for (int i = 0; i < lines.length; i++)
    {
      text("Score  " + (i + 1) + "   " +  updatedLines[i], width/2, height/2 - moveTextDown );
      moveTextDown -= 20;
    }
    //resets the move text down variable
    moveTextDown = 300.0f;
    //stops looping for static image
    noLoop();
  }
  //when game is finished new score is set to be checked
  void checkNewScore(int newScore)
  {
    int currentScore = newScore;//set new score to variable current score
    int previousScore = 0;
    print("Game Ooooovvveeeerrr     " ); 
    //check the array one line at a time
    for (int i = 0; i < lines.length; i++)
    {    
      if (currentScore >= intlines[i])//if the new score is bigger than the current score
      {
        previousScore = intlines[i];//save the old score in previous score
        intlines[i] = currentScore;//set the higher score to the current score
        currentScore = previousScore;
        print("You made a high score of  :" + newScore);
      }
      lines[i] = String.valueOf(intlines[i]);//convert back to strings for saving
      saveStrings("data/scores.txt", lines);//save back to external scores text document
      redraw();//as draw is static call redraw to update
    }
  }
}