int screenNumber = 0;
void drawIntroScreen()
{
  background(0);
  textFont(gameText, 20);
  textAlign(CENTER, TOP);  
  fill(255, 255);
  text("The Stroop Effect ", width/2, height/2-80.0f);
  text("Click P to play the game", width/2, height/2 -50.0f);
  text("Click I to view instructions", width/2, height/2 -20.0f);
  text("Click S to see local scoreboard", width/2, height/2 +10.0f);
  //draw buttons
  strokeWeight(0);
  rect(btn1X, btnY, btnSize, btnSize);
  rect(btn2X, btnY, btnSize, btnSize);
  rect(btn3X, btnY, btnSize, btnSize); 
  //draw text
  fill(0);
  text("I", btn1X+18, btnY+7); 
  text("P", btn2X+18, btnY+7);  
  text("S", btn3X+18, btnY+7); 
  noLoop();
}
void drawGameScreen()
{
  //plays ambient music
  backgroundMusic.play();
  //clock count before word changes
  drawScreenCount++;
  if (clearBackground == 0)
  {
    clearIntroScreen();
    clearBackground++;
  } else
  {
    if (drawScreenCount <= drawScreenCountLevel)//if count equals maximum count (different for each level)
    {
      if (level1Play)//if level 1
      {
        level1++; //level 1 counter
        if (level1 >= level1end)//level 1 counter equal or larger than maximum level 1 counter
        {
          drawScreenCountLevel = 30;//change word changing speed
          level1Play = false;//set level 1 to false
          level2Play = true;//set level 2 to true
        }
      }
      if (level2Play)
      {
        level2++;//level 2 counter
        if (level2 >= level2end)
        {
          drawScreenCountLevel = 20;//speed up words changing
          level2Play = false;
          level3Play = true;
        }
      }
      if (level3Play)
      {
        level3++;//level 3 counter
        if (level3 >= level3end)
        {
          drawScreenCountLevel = 10;//speed up words changing
          level3Play = false;
          gameOver = true;//game over
          
        }
      }
      //draw the background using the random screen number
      backgrounds[screenNumber].draw();
      //gets the colour of the word on the current screen using the random screen number
      backgroundWordColour = backgrounds[screenNumber].getScreenColour(screenNumber);
      //draw score
      textFont(gameText, 30);
      fill(255);
      text("Score :", 300.0f, 50.0f);
      fill(255);
      text(score, 400.0f, 50.0f);

      for (int i = 0; i < numOfWords; i++)
      {
        if (wordbut[i] == null)
        {
          //if there are no spots don't do anything
          print("Error, there are no words");
        } else
        {
          //run the draw function for the words and check mouse presses
          wordbut[i].draw();
          wordbut[i].mousePressed();
        }
      }
    } else
    {
      //reset the counter
      drawScreenCount = 0; 
      //picks a random word screen
      screenNumber = getRandomScreen.pickRandomScreen(numOfBackgrounds);
      //checks the colour of the word against the colour of the previous word
      colour = colourHandler.pickRandomColour(backgroundWordColour);
    }
  }
  if(gameOver)
  {
    //redraw the background
    background(0);
    //check the score against the scoreboard
    localScoreboard.checkNewScore(score);
    //draw the scoreboard
    drawLeaderboardScreen();
    //reset the level
    resetLevel();
  }
}
void resetLevel()
{
  //resets all the necessary variables including counters and speeds
    gameOver = false;
    level3Play = false;
    level1 = 0;
    level2 = 0; 
    level3 = 0;
    drawScreenCountLevel = 50;
    score = 0;
    level1Play = true;
}
void drawInstructionScreen()
{
  //draw button and text
  background(0);
  textFont(gameText, 20);
  textAlign(CENTER, TOP);
  fill(255, 255);
  text("Click B to go back", btn1X+100, btnY);
  strokeWeight(0);
  rect(btn3X, btnY, btnSize, btnSize);
  fill(0);
  text("B", btn3X+18, btnY+7); 
}
void drawLeaderboardScreen()
{
  //draw button and text
  background(0);
  textFont(gameText, 20);
  textAlign(CENTER, TOP);
  fill(255, 255);
  text("Click B to go back", btn1X+100, btnY);
  strokeWeight(0);
  rect(btn3X, btnY, btnSize, btnSize);
  fill(0);
  text("B", btn3X+18, btnY+7);  
  //draw scoreboard
  localScoreboard.draw();
}
void clearIntroScreen()
{
  background(0);
}