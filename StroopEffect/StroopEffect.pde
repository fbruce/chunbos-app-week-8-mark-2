//Stroop Effect - Lab 2 - Fiona Moore - B00241687

// Global variables
import processing.video.*;//needed to import and se video
Movie introMovie;
Maxim maxim;
AudioPlayer scoreSound, lossSound, backgroundMusic;
Colour colourHandler = new Colour();
RandomScreen getRandomScreen = new RandomScreen();
int numOfWords = 3;
int numOfBackgrounds = 3;
WordButton[] wordbut = new WordButton[numOfWords];
Background[] backgrounds = new Background[numOfBackgrounds];
int drawScreenCount = 0;
int currentScreen, score;
int clearBackground = 0;
PFont gameText;
float[] backgroundWordColour = {0, 0, 0};
float[] colour = {0, 0, 0};
int btn1X, btn2X, btn3X, btnY, btnSize;
Scoreboard localScoreboard;
int level1 = 0;//level counter
int level2 = 0;
int level3 = 0;
int level1end = 1000;//end of level counter
int level2end = 1000;
int level3end = 1000;
int drawScreenCountLevel = 50;//speed of word changes
boolean level1Play = true;//for changing levels
boolean level2Play = false;
boolean level3Play = false;
boolean gameOver = false;
boolean movieIsPlaying = false;
boolean scoreSoundPlayed = false;
boolean lossSoundPlayed = false;

void setup()
{
  size(480, 640);               // screen size
  background(0, 0, 0);    // background color
  imageMode(CENTER);            // image mode: center/ cornor
  maxim = new Maxim(this);
  scoreSound = maxim.loadFile("ding.wav");
  lossSound = maxim.loadFile("clang.wav");
  backgroundMusic = maxim.loadFile("mybeat.wav");
  score = 0;
  //creates special font
  gameText = createFont("Rockwell Bold", 30, true);

  for (int i = 0; i < numOfWords; i++)
  {
    wordbut[i] = new WordButton(i);
  }
  for (int i = 0; i < numOfBackgrounds; i++)
  {
    backgrounds[i] = new Background(i);
  }
  //sets up button variables
  btn1X = width/2 - 200;
  btnY= height - 70;
  btn2X= width/2-20; 
  btn3X=width/2 + 160; 
  btnSize = 40;

  localScoreboard = new Scoreboard();
  //loads video
  introMovie = new Movie(this, "IntroMovie.mp4");
}
void draw()
{
  switch(currentScreen)
  {
  case 0: 
    drawIntroScreen(); 
    break;
  case 1: 
    drawGameScreen(); 
    break;
  case 2:
    drawInstructionScreen();
    break;
  case 3:
    drawLeaderboardScreen();
    break;
  default : 
    background(0);
    break;
  }
  if (movieIsPlaying)
  {
    introMovie.play();
    //set position and size of movie
    image(introMovie, 240, 300, width-50, height-100);
  } else
  {
    introMovie.pause();
  }
}
void movieEvent(Movie m) { 
  m.read();
} 
void mousePressed()
{//checks if buttons on screens are pressed, either changes screen or plays video
  if ( mouseX< btn1X + btnSize && mouseX > btn1X && mouseY < btnY + btnSize && mouseY > btnY )
  {
    loop();
    currentScreen = 2;  
    movieIsPlaying = true;
  }
  if ( mouseX< btn2X + btnSize && mouseX > btn2X && mouseY < btnY + btnSize && mouseY > btnY )
  {
    loop();
    currentScreen = 1;
  }
  if ( mouseX< btn3X + btnSize && mouseX > btn3X && mouseY < btnY + btnSize && mouseY > btnY && currentScreen == 0)
  {
    loop();
    currentScreen = 3;
  } else if ( mouseX< btn3X + btnSize && mouseX > btn3X && mouseY < btnY + btnSize && mouseY > btnY)
  {
    currentScreen = 0; 
    movieIsPlaying = false;
    background(0);
    redraw();
  }
}