class WordButton
{
  float x, y;
  int scoreCount = 0;
  Colour colourFill = new Colour();
  float[] rgb = {0, 0, 0};
  float[] colourArray = {0, 0, 0};
  float[] wordPos = {0, 0};
  int wordCount = 0;
  String backgroundWord = " ";

  WordButton(int wordCount)
  {
    this.wordCount = wordCount;
    rgb = wordColour(wordCount);
    //set word string based on incoming wordCount number
    backgroundWord = word(wordCount);
    wordPos = setWordPosition(wordCount);
    x = wordPos[0];//random(0, width-10);
    y = wordPos[1]; //random(0, height-10+ (wordCount * 5));
  }
  void draw()
  {
    // background(0, 0, 0);
    textFont(gameText, 50);
    fill(rgb[0], rgb[1], rgb[2]);
    text(backgroundWord, wordPos[0], wordPos[1]);
  }
  String word(int ID)
  {
    switch(ID)
    {
    case 0: 
      return "Red";
    case 1: 
      return "Green";
    case 2: 
      return "Blue";
    case 3:
      return "Yellow";
    case 4:
      return "Purple";
    case 5:
      return "Turquoise";
    default: 
      return "error";
    }
  }
  float[] wordColour(int ID)
  {
    switch(ID)
    {
    case 0: 
      colourArray[0] = 255;
      colourArray[1] = 0.0;
      colourArray[2] = 0.0;
      return colourArray;
    case 1: 
      colourArray[0] = 0.0;
      colourArray[1] = 255;
      colourArray[2] = 0.0;
      return colourArray;
    case 2: 
      colourArray[0] = 0.0;
      colourArray[1] = 0.0;
      colourArray[2] = 255;
      return colourArray;
    case 3:
      colourArray[0] = 255;
      colourArray[1] = 255;
      colourArray[2] = 0.0;
      return colourArray;
    case 4:
      colourArray[0] = 128;
      colourArray[1] = 0;
      colourArray[2] = 128;
      return colourArray;
    case 5:
      colourArray[0] = 64;
      colourArray[1] = 224;
      colourArray[2] = 208;
      return colourArray;
    default: 
      return colourArray;
    }
  }
  float[] setWordPosition(int wordCount)
  {
    switch(wordCount)
    {
    case 0: 
      wordPos[0] = width/2;
      wordPos[1] = 400;
      return wordPos;
    case 1: 
      wordPos[0] = width/2;
      wordPos[1] = 450;
      return wordPos;
    case 2: 
      wordPos[0] = width/2;
      wordPos[1] = 500;
      return wordPos;
    case 3:
      wordPos[0] = 380;
      wordPos[1] = 400;
      return wordPos;
    case 4:
      wordPos[0] = 380;
      wordPos[1] = 450;
      return wordPos;
    case 5:
      wordPos[0] = 360;
      wordPos[1] = 500;
      return wordPos;
    default: 
      return wordPos;
    }
  }
  boolean checkWordCollision(int x, int y)
  {
    if (mouseX <= x+50 && mouseX>=x-50 && mouseY <= y+50 && mouseY>=y)
    {
      return true;
    }
    return false;
  }
  boolean checkColour()
  {
    //if get screen colour from screen
    if (backgroundWordColour[0] == 255.0 && colourArray[0] == 255.0)
    {
      return true;
      
    } else if (backgroundWordColour[1] == 255.0 && colourArray[1] == 255.0)
    {
      return true;
      
    } else if (backgroundWordColour[2] == 255.0 && colourArray[2] == 255.0)
    {
      return true;
    }
    return false;
  }
  void mousePressed()
  {
    if (scoreCount == 0)
    {
      if (checkWordCollision((int)x, (int)y) && mousePressed && checkColour())
      {
        fill(0);
        rect(400.0f, 26.0f, 40.0f, 50.0f);
        score++; 
        scoreCount = 1;
        scoreSound.setLooping(false);
        scoreSound.volume(1);
        scoreSound.play();
        delay(400);
        scoreCount = 0;
      } else if (checkWordCollision((int)x, (int)y) && mousePressed && !checkColour())
      {
        fill(0);
        rect(400.0f, 26.0f, 40.0f, 50.0f);
        score--; 
        scoreCount = 1;
        lossSound.setLooping(false);
        lossSound.volume(1);
        lossSound.play();
        delay(400);
        scoreCount = 0;
      } else
      {
        scoreCount = 0;
      }
    }
  }
  void delay(int delay)
  {
    int time = millis();
    while (millis() - time <= delay);
  }
}